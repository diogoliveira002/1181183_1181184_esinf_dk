package graphworld;

import graphworld.PairCapDist;
import graphworld.Utils;
import graphworld.World;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class WorldTest {

    private World instance;


    public WorldTest() throws IOException {
        this.instance = new World();
        List<String> p = Files.lines(Paths.get("paises.txt")).collect(Collectors.toList());
        List<String> f = Files.lines(Paths.get("fronteiras.txt")).collect(Collectors.toList());
        Utils.saveCountries(p, instance);
        Utils.saveBorders(f,instance);
    }

    @Test
    public void shortestPath() {
        System.out.println("--------- SHORTEST PATH EX 2------------");
        System.out.println("TESTE 1");
        System.out.println("PAÍS DE ORIGEM: PORTUGAL // PAÍS DE CHEGADA: ALEMANHA");
        LinkedList<String> expectedCapitals = new LinkedList<>();
        expectedCapitals.add("lisboa");
        expectedCapitals.add("madrid");
        expectedCapitals.add("paris");
        expectedCapitals.add("berlim");
        PairCapDist expected = new PairCapDist(expectedCapitals, 2434.0);
        System.out.println("Resultado esperado: \n"+ expected);
        PairCapDist result1 = instance.shortestPath("portugal","alemanha");
        System.out.println("Resultado obtido: \n" + result1);
        assertEquals(expected, result1);
    }

    @Test
    public void shortestPathWithMustPasses() {
        System.out.println("--------- SHORTEST PATH WITH MUST PASSES EX 3------------");
        System.out.println("TESTE 1");
        System.out.println("PAÍS DE ORIGEM: PORTUGAL // PAÍS DE CHEGADA: RÚSSIA // PAÍSES INTERMÉDIOS: ESPANHA, ALEMANHA, HOLANDA");
        /**System.out.println("Caminho mais curto entre Portugal e Espanha: \n");
        System.out.println(instance.shortestPath("portugal","espanha"));
        System.out.println("Caminho mais curto entre Espanha e Alemanha: \n");
        System.out.println(instance.shortestPath("espanha","alemanha"));
        System.out.println("Caminho mais curto entre Alemanha e Holanda: \n");
        System.out.println(instance.shortestPath("alemanha","holanda"));
        System.out.println("Caminho mais curto entre Holanda e Rússia: \n");
        System.out.println(instance.shortestPath("holanda","russia"));*/
        LinkedList<String> expectedCapitals = new LinkedList<>();
        expectedCapitals.add("lisboa");
        expectedCapitals.add("madrid");
        expectedCapitals.add("paris");
        expectedCapitals.add("bruxelas");
        expectedCapitals.add("amsterdam");
        expectedCapitals.add("berlim");
        expectedCapitals.add("varsovia");
        expectedCapitals.add("minsk");
        expectedCapitals.add("moscovo");
        PairCapDist expected = new PairCapDist(expectedCapitals, 4238.0);
        System.out.println("Resultado esperado: \n"+ expected);
        LinkedList<String> countries = new LinkedList<>();
        countries.add("letonia");
        countries.add("grecia");
        countries.add("holanda");
        countries.add("italia");
        PairCapDist result = instance.bestShortestPathWithMustPasses("franca", countries, "ucrania");
        System.out.println("Resultado obtido: \n"+result);
        assertEquals(expected, result);
    }

    @Test
    public void colorMap() {
        System.out.println("--------- COLOR MAP - EX 2------------");
        System.out.println("PRESENTING THE COLORS TO FRANCE AND ITS BORDERS");
        System.out.println("THE EXPECTED RESULT IS FOR EACH COUNTRY HAVING A DIFERENT COLOR FROM FRANCE ");
        Country country = instance.countryByName("franca");
        Iterable<Country> borders = instance.getWorldMap().adjVertices(country);
        instance.colorMap();
        System.out.println("França color : " + country.getColor());
        for(Country c : borders) {
            System.out.println("Country: " + c.getName() + " Color: " + c.getColor());
        }
    }

    @Test
    public void shortestCicle() {
        System.out.println("--------- BIGGEST CIRCUIT PASSING BY THE MAXIMUM NUMBER OF CAPITALS POSSIBLE - EX 5------------");
        System.out.println(("STARTING IN THE CAPITAL PARIS FROM FRANCE"));
        LinkedList<String> expectedCapitals = new LinkedList<>();
        expectedCapitals.add("paris");
        expectedCapitals.add("bruxelas");
        expectedCapitals.add("luxemburgo");
        expectedCapitals.add("berlim");
        expectedCapitals.add("praga");
        expectedCapitals.add("viena");
        expectedCapitals.add("bratislava");
        expectedCapitals.add("budapeste");
        expectedCapitals.add("zagreb");
        expectedCapitals.add("liubliana");
        expectedCapitals.add("roma");
        expectedCapitals.add("berna");
        expectedCapitals.add("paris");
        LinkedList<Country> result = instance.shortestCicle(instance.countryByName("lituania"));
        int i = 0;
        for(Country c : result){
            //System.out.println("EXPECTED RESULT:");
            //System.out.println(expectedCapitals.get(i));
            //System.out.println(("TRUE RESULT:"));
            System.out.println(result.get(i).getCapital());
            i++;
        }
    }
}