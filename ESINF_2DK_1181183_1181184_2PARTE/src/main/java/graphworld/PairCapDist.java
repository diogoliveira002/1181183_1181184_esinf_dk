package graphworld;

import java.util.LinkedList;
import java.util.Objects;

public class PairCapDist {

    LinkedList<String> capitals;
    Double distance;

    public PairCapDist(LinkedList<String> capitals, Double distance) {
        this.capitals = capitals;
        this.distance = distance;
    }

    public LinkedList<String> getCapitals() {
        return capitals;
    }

    public void setCapitals(LinkedList<String> capitals) {
        this.capitals = capitals;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PairCapDist that = (PairCapDist) o;
        return Objects.equals(capitals, that.capitals) &&
                Objects.equals(distance, that.distance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(capitals, distance);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PairCapDist{");
        sb.append("capitals=").append(capitals);
        sb.append(", distance=").append(distance);
        sb.append('}');
        return sb.toString();
    }
}
