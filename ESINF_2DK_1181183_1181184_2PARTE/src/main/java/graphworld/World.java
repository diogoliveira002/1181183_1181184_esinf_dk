package graphworld;

import graphbase.Edge;
import graphbase.Graph;
import graphbase.GraphAlgorithms;

import java.util.*;

public class World {

    private Graph<Country, Double> worldMap;

    /**
     * Constructor of World that instantiates worldMap.
     */
    public World() {
        //Directed: false, since the graph is undirected, if country1 is border with country2, country2 will be a border to country1 also.
        worldMap = new Graph<>(false);
    }

    /**
     * Method to insert country in vertex
     * @param country to be added
     */
    public void insertCountry(Country country) {
        worldMap.insertVertex(country);
    }

    /**
     * Method to insert border as edge
     * @param from country
     * @param to country
     * @param line value of the line of the txt file
     * @param distance distance between countries (weight of the edge)
     */
    public void insertBorder(Country from, Country to, Double line, Double distance) {
        worldMap.insertEdge(from, to, line, distance);
    }

    /**
     *
     * @return worldMap
     */
    public Graph<Country, Double> getWorldMap(){ return worldMap; }

    /**
     * get all countries added in the worldMap
     * @return Country[] list
     */
    public Country[] getAllCountries() {
        return worldMap.allkeyVerts();
    }

    /**
     * get num of edges in worldMap
     * @return int
     */
    public int getNumBorders() {
        return worldMap.numEdges();
    }

    /**
     * see if a country is a valid vertex
     * @param c country to be tested
     * @return true or false
     */
    public boolean isValid(Country c) {
        return worldMap.validVertex(c);
    }


    /**
     * search country by a given string
     * @param country string to search
     * @return null, if not found, or return Country
     */
    public Country countryByName(String country) {
        for(Country c : worldMap.allkeyVerts()) {
            if (c.getName().equalsIgnoreCase(country)) {
                return worldMap.allkeyVerts()[worldMap.getKey(c)];
            }
        }
        return null;
    }

    /**
     * Get shortest path from one country to another
     * @param vOrig country of origin
     * @param vDest country of destination
     * @return PairCapDist with the shortest path, with the capitals passed and the distance.
     */
    public PairCapDist shortestPath(String vOrig, String vDest) {
        PairCountriesDist allPaths = allPathsFromOneCountryToAnother(vOrig);
        ArrayList<LinkedList<Country>> paths = allPaths.getCountries();
        ArrayList<Double> dists = allPaths.getDistances();
        LinkedList<String> capitals = new LinkedList<>();
        Country dest = countryByName(vDest);
        int index = getIndexOfRightPath(dest, paths);
        if (index!=-1) {
            for (Country country : paths.get(index)) {
                capitals.add(country.getCapital());
            }
            PairCapDist result = new PairCapDist(capitals,dists.get(index));
            return result;
        } else {
            return null;
        }
    }

    /**
     * Get all shortest paths between country and all other vertex(countries)
     * @param vOrig country of origin
     * @return PairCountriesDist all paths and respective distance
     */
    private PairCountriesDist allPathsFromOneCountryToAnother(String vOrig) {
        ArrayList<LinkedList<Country>> paths = new ArrayList<>();
        ArrayList<Double> dists = new ArrayList<>();
        Country orig = countryByName(vOrig);
        GraphAlgorithms.shortestPaths(worldMap, orig,paths,dists);
        return new PairCountriesDist(paths, dists);
    }

    /**
     * tests which one is the best pass, considering permutation of the linked list must pass
     * @param vOrig country of origin
     * @param mustPass linked list of must passes countries
     * @param vDest country of destination
     * @return best pass - PairCapDist
     */
    public PairCapDist bestShortestPathWithMustPasses(String vOrig, LinkedList<String> mustPass, String vDest) {
        ArrayList<LinkedList<String>> allOrders = Utils.permute(mustPass);
        PairCapDist pMin = new PairCapDist(new LinkedList<>(), Double.MAX_VALUE);
        for (LinkedList<String> a : allOrders) {
            PairCapDist p = shortestPathWithMustPasses(vOrig, a, vDest);
            if (p.getDistance() < pMin.getDistance()) {
                pMin = p;
            }
        }
        return pMin;
    }

    /**
     * creates shortest path, with static order.
     * @param vOrig country of origin
     * @param mustPass linked list of must passses countries
     * @param vDest country of destination
     * @return short pass - PairCapDist
     */
    public PairCapDist shortestPathWithMustPasses(String vOrig, LinkedList<String> mustPass, String vDest) {
        PairCapDist p1 = shortestPath(vOrig, mustPass.get(0));
        LinkedList<String> capitals = p1.getCapitals();
        Double distance = p1.getDistance();
        int size = mustPass.size();
        for (int i = 0; i < size - 1; i++) {
            PairCapDist p = shortestPath(mustPass.get(i),mustPass.get(i+1));
            p.capitals.removeFirst();
            capitals.addAll(p.capitals);
            distance += p.getDistance();
        }
        PairCapDist p2 = shortestPath(mustPass.getLast(), vDest);
        p2.capitals.removeFirst();
        capitals.addAll(p2.capitals);
        distance += p2.getDistance();

        return new PairCapDist(capitals, distance);
    }


    /**
     * return index of right pass
     * @param dest country of destination
     * @param paths list of shortest pass from country to all others
     * @return index of the path in which the country in the last position is the country of destination
     */
    private int getIndexOfRightPath(Country dest, ArrayList<LinkedList<Country>> paths) {
        for (LinkedList<Country> countries: paths) {
            if (!countries.isEmpty()) {
                if (countries.getLast().equals(dest)) {
                    return paths.indexOf(countries);

                }
            }
        }
        return -1;
    }

    /**
     * Fills all the countries of the map with a respective color
     */
    public void colorMap(){
        int i = 0;
        int j = 0;
        Colors colors1[] = {Colors.Yellow, Colors.Blue, Colors.Green, Colors.Red, Colors.Brown};
        for(Country c : worldMap.allkeyVerts()){
            j = worldMap.outDegree(c);
            Iterable <Country> edges = worldMap.adjVertices(c);
            colorMapAux(c, edges, colors1, i, j);
            i = 0;
        }
    }

    /**
     * Auxiliar method to colormap method that paints the country with the right color considering the colors of border countries
     * @param country country that will be painted
     * @param edges List of the edges of the country
     * @param colors ArrayList of colors available
     * @param i auxiliar variable
     * @param j auxiliar variable
     */
    private void colorMapAux(Country country, Iterable <Country> edges, Colors colors[], int i, int j){
        int count = 0;
        Iterator<Country> it = edges.iterator();
        country.setColor(colors[i]);
        for(int k = 0; k < j; k++){
            if(country.getColor().equals(it.next().getColor())){
                count ++;
            }
        }
        if(count != 0){
            i++;
            colorMapAux(country, edges, colors, i, j);
        }
    }

    /**
     * return a list with the cicle of the countries
     * @param vOrig source country
     * @return returns the list of countries that were passed by, starting and finishing the cicle in the same source country
     */
    public LinkedList<Country> shortestCicle(Country vOrig){
        LinkedList<Country> cicleList = new LinkedList<>();
        LinkedList<Country> availableCountries = GraphAlgorithms.DepthFirstSearch(worldMap,vOrig);
        cicleList.add(vOrig);
        shortestCicleAux(vOrig, availableCountries, cicleList, vOrig);
        return cicleList;
    }

    /**
     * Recursive method that calls him self adding each time the next best country to go while it doesnt get to the source country
     * @param country country that has the borders countries that can be the next destination
     * @param availableCountries list of available countries to go
     * @param cicleList list that saves the countries of the cicle that will be filled
     * @param vOrig source country
     */
    private void shortestCicleAux(Country country, LinkedList<Country> availableCountries, LinkedList<Country> cicleList, Country vOrig){
        int aux = 0;
        double pathSize = Double.MAX_VALUE;
        Iterable<Edge<Country, Double>> edges = worldMap.outgoingEdges(country);
        int nEdges = shortestCicleAux2(edges, availableCountries, vOrig);
        if(nEdges > 0){
            for(Edge<Country, Double> edge : edges) {
                Country destCount = edge.getVDest();
                if (!destCount.equals(vOrig) && availableCountries.contains(destCount)) {
                    if (edge.getWeight() < pathSize) {
                        pathSize = edge.getWeight();
                        if (aux != 0 ) {
                            availableCountries.add(cicleList.getLast());
                            cicleList.removeLast();
                            cicleList.add(destCount);
                            availableCountries.remove(destCount);
                            aux++;
                        } else if (aux == 0) {
                            cicleList.add(destCount);
                            availableCountries.remove(destCount);
                            aux++;
                        }
                    }
                }else if(destCount.equals(vOrig) && nEdges == 1){
                    cicleList.add(destCount);
                    aux++;
                }
            }
        }else {
            cicleList.removeLast();
            availableCountries.remove(country);
            country = cicleList.getLast();
            shortestCicleAux(country, availableCountries, cicleList, vOrig);
        }

        if(!cicleList.getLast().equals(vOrig)) {
            country = cicleList.getLast();
            shortestCicleAux(country, availableCountries, cicleList, vOrig);
        }
    }

    /**
     * Returns the number of edges of a country that are available to go through
     * @param edges list of the edges of the country vertex
     * @param availableCountries list of available countries to go
     * @param vOrig source country
     * @return number of edges of a country that are available to go through
     */
    private Integer shortestCicleAux2(Iterable<Edge<Country, Double>> edges, LinkedList<Country> availableCountries, Country vOrig){
        int count = 0;
        for(Edge<Country, Double> edge : edges){
            Country country = edge.getVDest();
            if(availableCountries.contains(country) || country.equals(vOrig)){
                count++;
            }
        }

        return count;
    }

}
