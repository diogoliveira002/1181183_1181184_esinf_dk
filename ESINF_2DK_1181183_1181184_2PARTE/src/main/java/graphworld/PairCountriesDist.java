package graphworld;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Objects;

public class PairCountriesDist {
    private ArrayList<LinkedList<Country>> countries;
    private ArrayList<Double> distances;


    public PairCountriesDist(ArrayList<LinkedList<Country>> countries, ArrayList<Double> distances) {
        this.countries = countries;
        this.distances = distances;
    }

    public ArrayList<LinkedList<Country>> getCountries() {
        return countries;
    }

    public ArrayList<Double> getDistances() {
        return distances;
    }

    public void setCountries(ArrayList<LinkedList<Country>> countries) {
        this.countries = countries;
    }

    public void setDistances(ArrayList<Double> distances) {
        this.distances = distances;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PairCountriesDist that = (PairCountriesDist) o;
        return Objects.equals(countries, that.countries) &&
                Objects.equals(distances, that.distances);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countries, distances);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PairCountriesDist{");
        sb.append("countries=").append(countries);
        sb.append(", distances=").append(distances);
        sb.append('}');
        return sb.toString();
    }
}
