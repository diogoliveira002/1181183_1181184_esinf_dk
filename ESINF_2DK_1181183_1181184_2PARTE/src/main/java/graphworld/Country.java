package graphworld;

import java.util.Objects;

public class Country {

    private String name;
    private String capital;
    private String continent;
    private double population;
    private Coordinates coordinates;
    private Colors color;

    public Country(String name, String continent, String capital, double population, Coordinates coordinates, Colors color) {
        this.name = name;
        this.continent = continent;
        this.capital = capital;
        this.population = population;
        this.coordinates = coordinates;
        this.color = color;
    }

    public String getName(){
        return name;
    }

    public String getCapital(){
        return capital;
    }

    public double getPopulation(){
        return population;
    }

    public Coordinates getCoordinates(){
        return coordinates;
    }
    
    public Colors getColor(){
        return color;
    }
    
    public void setName(String name){
        this.name = name;
    }

    public void setCapital(String capital){
        this.capital = capital;
    }

    public void setPopulation(double population){
        this.population = population;
    }

    public void setCoordinates(float longitude, float latitude){
        this.coordinates.setLongitude(longitude);
        this.coordinates.setLatitude(latitude);

    }

    public void setColor(Colors color) {
        this.color = color;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.name);
        hash = 67 * hash + Objects.hashCode(this.capital);
        hash = 67 * hash + Objects.hashCode(this.continent);
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.population) ^ (Double.doubleToLongBits(this.population) >>> 32));
        hash = 67 * hash + Objects.hashCode(this.coordinates);
        hash = 67 * hash + Objects.hashCode(this.color);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Country other = (Country) obj;
        if (Double.doubleToLongBits(this.population) != Double.doubleToLongBits(other.population)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.capital, other.capital)) {
            return false;
        }
        if (!Objects.equals(this.continent, other.continent)) {
            return false;
        }
        if (!Objects.equals(this.coordinates, other.coordinates)) {
            return false;
        }
        if (this.color != other.color) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Country{" + "name=" + name + ", capital=" + capital + ", continent=" + continent + ", population=" + population + ", coordinates=" + coordinates + ", color=" + color + '}';
    }
    

}
