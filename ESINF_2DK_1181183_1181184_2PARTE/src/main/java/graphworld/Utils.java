package graphworld;

import java.util.*;

public class Utils {


    /**
     * Fill worldMap with countries from given list (Create vertex)
     * @param countryList list of countries from txt file
     * @param worldMap instance of map
     */
    public static void saveCountries(List<String> countryList, World worldMap) {
        for(int i = 0; i < countryList.size(); i++){
            String []aux1 = countryList.get(i).trim().split(",");
            trimArray(aux1);
            Coordinates coordinates = new Coordinates(Float.parseFloat(aux1[4]), Float.parseFloat(aux1[5]));
            Country country = new Country(aux1[0],aux1[1], aux1[3], Double.parseDouble(aux1[2]), coordinates, Colors.White);
            worldMap.insertCountry(country);
            }
        }

    /**
     * Fill worldMap with borders (create edges)
     * @param bordersList list of borders from txt file
     * @param worldMap instance of map
     */
    public static void saveBorders(List<String> bordersList, World worldMap) {
        ListIterator<String> li = bordersList.listIterator();
        Double line = Double.valueOf(0);
        while (li.hasNext()) {
            String[] aux = li.next().trim().split(",");
            trimArray(aux);
            Country country1 = worldMap.countryByName(aux[0]);
            Country country2 = worldMap.countryByName(aux[1]);
            worldMap.insertBorder(country1, country2, line, calculateDistance(country1.getCoordinates().getLatitude(), country1.getCoordinates().getLongitude(), country2.getCoordinates().getLatitude(), country2.getCoordinates().getLongitude()));
            line++;
        }
    }

    /**
     * calculate distance between two points in the world, given their latitudes and longitudes
     * @param lat1 latitude from country1
     * @param lon1 longitude from country1
     * @param lat2 latitude from country2
     * @param lon2 longitude from country2
     * @return distance between country1 and country 2, in kilometers
     */
    public static double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        final double R = 6371e3;
        double theta1 = Math.toRadians(lat1);
        double theta2 = Math.toRadians(lat2);
        double deltaTheta = Math.toRadians(lat2 - lat1);
        double deltaLambda = Math.toRadians(lon2 - lon1);
        double a = Math.sin(deltaTheta / 2) * Math.sin(deltaTheta / 2) + Math.cos(theta1)
                * Math.cos(theta2) * Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c; //distância em metros
        return Math.round(d * Math.pow(10, -3));// devolve em quilometros
    }


    /**
     * trim blank spaces from array. Example "portugal " to "portugal"
     * @param array some array to trim spaces
     */
    public static void trimArray(String[] array) {
        for (int i = 0; i < array.length; i++)
            array[i] = array[i].trim();
    }

    /**
     * method to permute any linked list, based on heap algorithm
     * @param countries linked list of countries (in this case, the must pass ones)
     * @return all possible paths, changing the order of all the paramenters in the linked list
     */
    public static ArrayList<LinkedList<String>> permute(LinkedList<String> countries) {
        ArrayList<LinkedList<String>> result = new ArrayList<>();

        //start from an empty list
        result.add(new LinkedList<>());

        for (int i = 0; i < countries.size(); i++) {
            //list of list in current iteration of the array num
            ArrayList<LinkedList<String>> current = new ArrayList<>();

            for (LinkedList l : result) {
                // # of locations to insert is largest index + 1
                for (int j = 0; j < l.size()+1; j++) {
                    // + add num[i] to different locations
                    l.add(j, countries.get(i));

                    LinkedList<String> temp = new LinkedList<>(l);
                    current.add(temp);

                    // - remove num[i] add
                    l.remove(j);
                }
            }

            result = new ArrayList<>(current);
        }

        return result;
    }



}
