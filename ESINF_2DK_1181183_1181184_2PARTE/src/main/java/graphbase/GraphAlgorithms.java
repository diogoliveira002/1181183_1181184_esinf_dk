package graphbase;

import java.util.ArrayList;
import java.util.LinkedList;

public class GraphAlgorithms {

    protected static<V,E> void shortestPathLength(Graph<V,E> g, V vOrig, V[] vertices,
                                                  boolean[] visited, int[] pathKeys, double[] dist){

        int vert = g.getKey(vOrig);
        dist[vert] = 0;                    //distancia ao primeiro vertice 0
        while (vert != -1) {               // enquanto o vertice nao é invalido
            visited[vert] = true;
            for (Edge<V, E> edge : g.outgoingEdges(vOrig)) {         // todos os edges que saem do vertice
                V vAdj = g.opposite(vOrig, edge);                    // todos os edges que entram no vertice
                int keyAdj = g.getKey(vAdj);
                if (visited[keyAdj] == false && dist[keyAdj] > dist[g.getKey(vOrig)] + edge.getWeight()) { //se o adj ainda nao foi visitado e a distanicia ao ao keyAdj for maior que a distanciao ao vOrig+o weight dess edge
                    dist[keyAdj] = dist[g.getKey(vOrig)] + edge.getWeight(); //a diastancia do adj toma o valor da distancia do vorig +  weight do edge
                    pathKeys[keyAdj] = g.getKey(vOrig);            //o valor que esta no pathkeys é o valor da chave do vertice de origem
                }
            }
            vert = -1;      //vertice invalido
            Double aux = Double.MAX_VALUE;   //maior valor possivel
            for (int i = 0; i < visited.length; i++) {   //para cada visitado
                if (visited[i] == false && dist[i] < aux) {// se nao foi visitado e se a distancia for menor que o o valor que esta na auxiliar
                    aux = dist[i];          //a auxiliar toma o valor da distancia do vertice e que nos encontramos
                    vert = i;                //ver passa a ser o vertice em que nos encontramos
                    vOrig = g.allkeyVerts()[i];    //o vertice guarda todas as keys do vertice em que nos encontramos
                }
            }
        }
    }

    protected static<V,E> void getPath(Graph<V,E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path){

        int vert = g.getKey(vDest);
        while (vert != -1) {
            path.addFirst(verts[vert]);                               //coloca o vertice que queremos na lista de path
            vert = pathKeys[vert];                                          //vai buscar o vertice anterior
        }
    }

    public static<V,E> boolean shortestPaths(Graph<V,E> g, V vOrig, ArrayList<LinkedList<V>> paths, ArrayList <Double> dists ){

        if (!g.validVertex(vOrig)) return false;

        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts]; //default value: false
        int[] pathKeys = new int[nverts];
        double[] dist = new double [nverts];
        V[] vertices = g.allkeyVerts();

        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }

        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);

        dists.clear(); paths.clear();
        for (int i = 0; i < nverts; i++) {
            paths.add(null);
            dists.add(null);
        }
        for (int i = 0; i < nverts; i++) {
            LinkedList <V> shortPath = new LinkedList<>();
            if (dist[i]!=Double.MAX_VALUE)
                getPath(g,vOrig,vertices[i],vertices,pathKeys,shortPath);
            paths.set(i, shortPath);
            dists.set(i, dist[i]);
        }
        return true;
    }

    /**
     * Performs depth-first search starting in a Vertex
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static<V,E> void DepthFirstSearch(Graph<V,E> g, V vOrig, boolean[] visited, LinkedList<V> qdfs){

        qdfs.add(vOrig);                 //adiciona o vertice de origem à linkedList
        visited[g.getKey(vOrig)] = true;   //marcamos o vertice de origem como já visistado
        for (V vadj : g.adjVertices(vOrig)) {    //percorremos todos os vertices adjacentes desse vertice
            if (visited[g.getKey(vadj)] != true) {  //se o vertice ainda nao foi visistado
                DepthFirstSearch(g, vadj, visited, qdfs);  //voltamos a chamar o metodo
            }
        }
    }

    /**
     * @param g Graph instance
     * @param vert information of the Vertex that will be the source of the search
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static<V,E> LinkedList<V> DepthFirstSearch(Graph<V,E> g, V vert){

        if(!g.validVertex(vert)){
            return null;
        }

        LinkedList<V> qdfs = new LinkedList<>();

        boolean [] visited = new boolean[g.numVertices()];
        visited[g.getKey(vert)] = true;

        DepthFirstSearch(g, vert, visited, qdfs);

        return qdfs;
    }
}
