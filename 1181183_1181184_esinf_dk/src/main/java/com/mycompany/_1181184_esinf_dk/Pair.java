package com.mycompany._1181184_esinf_dk;

import java.util.Comparator;
import java.util.Objects;

public class Pair implements Comparable<Pair>{
    private double population;
    private Country country;

    public Pair(Double population, Country country)
    {
        this.population = population;
        this.country = country;
    }

    public double getPopulation() {
        return population;
    }

    public Country getCountry() {
        return country;
    }
    
    public void setPopulation(double population){
        this.population = population;
    }
    
    public void setCountry(Country country){
        this.country = country;
    }
    
    
    @Override
    public String toString() {
        return "Pair{" + "population=" + population + ", country=" + country + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair pair = (Pair) o;
        return Double.compare(pair.population, population) == 0 &&
                Objects.equals(country, pair.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(population, country);
    }

    @Override
    public int compareTo(Pair o) {
        if (this.population < o.getPopulation())  {
            return -1;
        } else if (this.population == o.getPopulation()) {
            return 0;
        } else {
            return 1;
        }
    }
}