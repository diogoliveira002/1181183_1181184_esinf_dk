package com.mycompany._1181184_esinf_dk;

import java.util.*;

/**
 *
 * @author diogo
 */
public class World{
    
    private Map <Country, Set<Country>> m;
    private Set<Continent> continents;

    World() {
        m = new HashMap<>();
        continents = new HashSet<>();
    }
    
    //Guarda a informação de cada continente presente no ficheiro
    void saveContinents(List<String> continentsList) {
        for (int i = 0; i < continentsList.size() ; i++) {
            String[] aux = continentsList.get(i).trim().split(",");
            Continent continent = new Continent(aux[1]);
            continents.add(continent);
        }
    }
    
    //Guarda a informação dos países nos seus respetivos continentes
    void saveCountriesInContinents(List<String> listaPaises){
        for(int i = 0; i < listaPaises.size(); i++){
            String []aux1 = listaPaises.get(i).trim().split(",");
            Coordinates coordinates = new Coordinates(Float.parseFloat(aux1[4]), Float.parseFloat(aux1[5]));
            Country country = new Country(aux1[0], aux1[3], Double.parseDouble(aux1[2]), coordinates);
            for (Continent c : continents) {
                if (c.getName().equalsIgnoreCase(aux1[1])) {
                    c.addCountry(country);
                }
            }
        }
    }
    
    //Guarda num map os países e as suas respetivas fronteiras
    void saveBorders(List<String> listaFronteiras){
        ListIterator<String> li = listaFronteiras.listIterator();
        Set<Country> set = new TreeSet<>();
        while (li.hasNext()) {
            String[] aux = li.next().trim().split(",");
            set = new TreeSet<>();
            if (!m.containsKey(searchCountryByName(aux[0]))) {
                m.put(searchCountryByName(aux[0]),set);
            }
            for(Map.Entry<Country, Set<Country>> entry : m.entrySet()) {
                if (entry.getKey().getName().equalsIgnoreCase(aux[0])) {
                    entry.getValue().add(searchCountryByName(aux[1].trim()));
                }
            }
        }

    }
    
    //Devolve os continentes existentes
    public Set<Continent> getContinents(){
        return continents;
    }
    
    //Devolve a instância de um país através do nome
    public Continent getContinentByName(String cont){
        for(Continent c : continents) {
            if (c.getName().equalsIgnoreCase(cont)) {
                return c;
            }
        }
        return null;
    }
    
    //Verifica se um determinado país pertence a um determinado continente
    public boolean belongsToContinent(String continent, Country country) {
       for(Continent c:continents) {
           if(c.getName().equalsIgnoreCase(continent)) {
               if(c.getCountries().contains(country)) {
                   return true;
               }
           }
       }
       return false;
    }
    
    //Retorna uma lista com a populaçã e o seu respetivo país por ordem crescente desde que esta população seja maior que o valor recebido como parâmetro e que pertença ao continente recebido no parâmetro
    public List<Pair> populationHigherThan(double n, Continent continent) {
        Pair aux;
        List<Pair> countryPop = new ArrayList<Pair>();
        for (Continent cont : continents) {
            if (cont.equals(continent)) {
                for (Country c : cont.getCountries()) {
                    if (c.getPopulation() > n) {
                        countryPop.add(new Pair(c.getPopulation(), c));
                    }
                }
            }
        }
        return countryPop;
    }
    
    //Devolve um map de um determinado país e da quantidade de fronteiras, recebendo o seu respetivo continente
    private Map<Country,Integer> numberOfCountryBorders(String continent) {
        Map<Country,Integer>  a = new HashMap<>();
                for (Map.Entry<Country, Set<Country>> entry : m.entrySet()) {
                    if (belongsToContinent(continent, entry.getKey())) {
                        a.put(entry.getKey(), entry.getValue().size());
                    }
                }
        return a;
    }

    //Recebe a string do continente a ser trabalhado como parãmetro e ordena por número de fronteiras os seus países
    public Map<Integer,Set<Country>> numberOfCountryBorders2(String continent) {
        Map<Integer,Set<Country>>  a = new TreeMap<Integer,Set<Country>>(Collections.reverseOrder());
        Map<Country, Integer> borders = numberOfCountryBorders(continent);
        Set countries = new TreeSet();
        for (Map.Entry<Country, Integer> entry : borders.entrySet()) {
            countries = new TreeSet();
            if (!a.containsKey(entry.getValue())) {
                for (Map.Entry<Country, Integer> entry1 : borders.entrySet()) {
                    if (entry.getValue() == entry1.getValue()) {
                        countries.add(entry1.getKey());
                    }
                }
                a.put(entry.getValue(), countries);
            }
        }
        return a;
    }
    
    
    //Procura a instância de um país recebendo como parâmetro o seu nome
    Country searchCountryByName(String country) {
        for(Continent continent: continents) {
            for(Country c: continent.getCountries()) {
                if(country.equalsIgnoreCase(c.getName())) {
                    return c;
                }
            }
        }
        return null;
    }
    
    //Devolve a quantidade de fronteiras miníma que é necessário ultrapassar para ir do país source ao país destiny
    int getShortestWay(Country source, Country destiny){
        int count = 0;
        List<Country> countries = new ArrayList<>();
        Map<Country, Integer> nFronteiras = new HashMap<>();
        countries.add(source);
        nFronteiras.put(source, count);
        for(int i = 0; i < countries.size(); i++){
            Country country = countries.get(i);
            if(countries.get(0).equals(destiny)){
                return 0;
            }
            if(nFronteiras.containsKey(destiny)){
                return nFronteiras.get(countries.get(count)) +1;
            }
            count++;
            Set<Country> setC = getBordersAux(country);
            for(Country c : setC){
                if(!nFronteiras.containsKey(c)){
                    countries.add(c);
                    nFronteiras.put(c, nFronteiras.get(country)+1); 
               }                         
            }
        }
        return 0;
    }   
    
    //Devolve todas as fronteiras que a instância recebida em parâmetro pode ter
    private Set<Country> getBordersAux(Country country){
       Set<Country> set = new HashSet<>();
       for(Map.Entry<Country, Set<Country>> entry : m.entrySet()){
            if(country.equals(entry.getKey())){
                for(Country c1 : entry.getValue()){
                    set.add(c1);
                }
            }
            for(Country c2 : entry.getValue()){
                if (country.equals(c2)){
                    set.add(entry.getKey());
                }
            }
        }
        return set;
    }
  
}
