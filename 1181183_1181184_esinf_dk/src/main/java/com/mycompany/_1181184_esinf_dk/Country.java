/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany._1181184_esinf_dk;

import java.util.Objects;

/**
 *
 * @author diogo
 */
public class Country implements Comparable<Country> {
    
    private String name;
    private String capital;
    private double population;
    private Coordinates coordinates;

    public Country(String name, String capital, double population, Coordinates coordinates) {
        this.name = name;
        this.capital = capital;
        this.population = population;
        this.coordinates = coordinates;
    }
    
    public String getName(){
        return name;
    }
    
    public String getCapital(){
        return capital;
    }
    
    public double getPopulation(){
        return population;
    }
    
    public Coordinates getCoordinates(){
        return coordinates;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setCapital(String capital){
        this.capital = capital;
    }
    
    public void setPopulation(double population){
        this.population = population;
    }
    
    public void setCoordinates(float longitude, float latitude){
        this.coordinates.setLongitude(longitude);
        this.coordinates.setLatitude(latitude);
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.capital);
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.population) ^ (Double.doubleToLongBits(this.population) >>> 32));
        hash = 97 * hash + Objects.hashCode(this.coordinates);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Country other = (Country) obj;
        if (Double.doubleToLongBits(this.population) != Double.doubleToLongBits(other.population)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.capital, other.capital)) {
            return false;
        }
        if (!Objects.equals(this.coordinates, other.coordinates)) {
            return false;
        }
        return true;
    }
    
    @Override
    public int compareTo(Country t) {
        return this.name.compareTo(t.name);
    }

    @Override
    public String toString() {
        return name;
        //return "Country{" + "name=" + name + ", capital=" + capital + ", population=" + population + ", coordinates=" + coordinates + '}';
    }
    
    
}
