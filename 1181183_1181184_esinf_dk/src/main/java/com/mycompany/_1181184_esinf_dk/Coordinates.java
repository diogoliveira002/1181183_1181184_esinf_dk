/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany._1181184_esinf_dk;

/**
 *
 * @author diogo
 */
public class Coordinates implements Comparable<Coordinates> {
    
    private float longitude;
    private float latitude;

    public Coordinates(float longitude, float latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }
    
    public float getLongitude(){
        return longitude;
    }
    
    public float getLatitude(){
        return latitude;
    }
    
    public void setLongitude(float longitude){
        this.longitude = longitude;
    }
    
    public void setLatitude(float latitude){
        this.latitude = latitude;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Float.floatToIntBits(this.longitude);
        hash = 73 * hash + Float.floatToIntBits(this.latitude);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coordinates other = (Coordinates) obj;
        if (Float.floatToIntBits(this.longitude) != Float.floatToIntBits(other.longitude)) {
            return false;
        }
        if (Float.floatToIntBits(this.latitude) != Float.floatToIntBits(other.latitude)) {
            return false;
        }
        return true;
    }
    
    @Override
    public int compareTo(Coordinates t) {
        return this.compareTo(t);
    }

    @Override
    public String toString() {
        return "Coordinates{" + "longitude=" + longitude + ", latitude=" + latitude + '}';
    }
    

    
    
}
