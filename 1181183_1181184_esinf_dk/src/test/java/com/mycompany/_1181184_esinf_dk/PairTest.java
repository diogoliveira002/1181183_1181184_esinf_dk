/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany._1181184_esinf_dk;

import com.sun.org.apache.bcel.internal.generic.AALOAD;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author diogo
 */
public class PairTest {
    
    public PairTest() {
    }

    @Test
    public void compareTo1() {
        System.out.println("compareTo1");
        Country c1 = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        double p1 = c1.getPopulation();
        Pair pair1 = new Pair(p1, c1);
        Country c2 = new Country("Alemanha", "Berlim", 60.1, new Coordinates(132.132f,21.2f));
        double p2 = c2.getPopulation();
        Pair pair2 = new Pair(p2,c2);
        int result = pair1.compareTo(pair2);
        assertTrue(result<0);
    }

    @Test
    public void compareTo2() {
        System.out.println("compareTo2");
        Country c1 = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        double p1 = c1.getPopulation();
        Pair pair1 = new Pair(p1, c1);
        Country c2 = new Country("Alemanha", "Berlim", 60.1, new Coordinates(132.132f,21.2f));
        double p2 = c2.getPopulation();
        Pair pair2 = new Pair(p2,c2);
        int result = pair1.compareTo(pair2);
        assertTrue(result>0);
    }

    @Test
    public void compareTo3() {
        System.out.println("compareTo3");
        Country c1 = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        double p1 = c1.getPopulation();
        Pair pair1 = new Pair(p1, c1);
        Country c2 = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        double p2 = c2.getPopulation();
        Pair pair2 = new Pair(p2,c2);
        int result = pair1.compareTo(pair2);
        assertTrue(result==0);
    }
    
    @Test
    void equals01() {
        System.out.println("equals01");
        Country c1 = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        double p1 = c1.getPopulation();
        Pair pair1 = new Pair(p1, c1);
        Country c2 = new Country("Alemanha", "Berlim", 60.1, new Coordinates(132.132f,21.2f));
        double p2 = c2.getPopulation();
        Pair pair2 = new Pair(p2,c2);
        boolean expResult = false;
        boolean result = pair1.equals(pair2);
        Assert.assertEquals(expResult, result);
    }
    
    @Test
    void equals02() {
        System.out.println("equals02");
        Country c1 = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        double p1 = c1.getPopulation();
        Pair pair1 = new Pair(p1, c1);
        Country c2 = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        double p2 = c2.getPopulation();
        Pair pair2 = new Pair(p2,c2);
        boolean expResult = true;
        boolean result = pair1.equals(pair2);
        Assert.assertEquals(expResult, result);
    }
}
