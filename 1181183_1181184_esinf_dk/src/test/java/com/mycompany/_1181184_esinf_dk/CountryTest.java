package com.mycompany._1181184_esinf_dk;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CountryTest {



    @Test
    void compareTo1() {
        System.out.println("compareTo1");
        Country c = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        Country instance = new Country("Alemanha", "Berlim", 60.1, new Coordinates(132.132f,21.2f));
        int result = instance.compareTo(c);
        assertTrue(result<0);
    }

    @Test
    void compareTo2() {
        System.out.println("compareTo2");
        Country instance = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        Country c = new Country("Alemanha", "Berlim", 60.1, new Coordinates(132.132f,21.2f));
        int result = instance.compareTo(c);
        assertTrue(result>0);
    }

    @Test
    void compareTo3() {
        System.out.println("compareTo3");
        Country c = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        Country instance =  new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        int result = instance.compareTo(c);
        assertTrue(result==0);
    }

    @Test
    void equals01() {
        Object obj = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        Country c = new Country("Alemanha", "Berlim", 60.1, new Coordinates(132.132f,21.2f));
        boolean expResult = false;
        boolean result = c.equals(obj);
        Assert.assertEquals(expResult, result);
    }

    @Test
    void equals02() {
        Object obj = new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        Country instance =  new Country("Portugal", "Lisboa", 10.1, new Coordinates(132.232f,421.2f));
        boolean expResult = true;
        boolean result = instance.equals(obj);
        Assert.assertEquals(expResult, result);
    }
}