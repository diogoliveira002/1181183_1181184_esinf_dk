/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany._1181184_esinf_dk;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;


public class WorldTest {
    private World instance;
    
    public WorldTest() throws IOException {
        instance = new World();
        List<String> p = Files.lines(Paths.get("paises.txt")).collect(Collectors.toList());
        List<String> f = Files.lines(Paths.get("fronteiras.txt")).collect(Collectors.toList());
        instance.saveContinents(p);
        instance.saveCountriesInContinents(p);
        instance.saveBorders(f);
    }

    @Test
    public void populationHigherThan() {
        Double population = 50.0;
        Continent continent = instance.getContinentByName(" europa");
        List<Pair> result = this.instance.populationHigherThan(population,continent);
        List<Pair> expected = new ArrayList<>();
        expected.add(new Pair(82.8, instance.searchCountryByName("alemanha")));
        expected.add(new Pair(66.99, instance.searchCountryByName("franca")));
        expected.add(new Pair(60.59, instance.searchCountryByName("italia")));
        expected.add(new Pair(65.81, instance.searchCountryByName("reinounido")));
        expected.add(new Pair(146.5, instance.searchCountryByName("russia")));
        expected.add(new Pair(79.81, instance.searchCountryByName("turquia")));
        Assert.assertEquals(expected, result);
    }

    @Test
    public void numberOfCountryBorders2() {
        Map<Integer, Set<Country>> expected = new TreeMap<>(Collections.reverseOrder());;
        Set<Country> countries = new TreeSet<>();
        countries.add(instance.searchCountryByName("argentina"));
        expected.put(5, countries);
        countries = new TreeSet<>();
        countries.add(instance.searchCountryByName("bolivia"));
        expected.put(4,countries);
        countries = new TreeSet<>();
        countries.add(instance.searchCountryByName("brasil"));
        expected.put(8, countries);
        countries = new TreeSet<>();
        countries.add(instance.searchCountryByName("chile"));
        countries.add(instance.searchCountryByName("equador"));
        countries.add(instance.searchCountryByName("guianafrancesa"));
        expected.put(1, countries);
        countries = new TreeSet<>();
        countries.add(instance.searchCountryByName("colombia"));
        expected.put(3, countries);
        countries = new TreeSet<>();
        countries.add(instance.searchCountryByName("guiana"));
        expected.put(2, countries);
        Map<Integer, Set<Country>> result = instance.numberOfCountryBorders2(" americasul");
        Assert.assertEquals(expected, result);
    }
    
    @Test
    public void getShortestWay(){
        int i;
        int expectedInt = 2;
        i = instance.getShortestWay(instance.getContinentByName(" europa").getCountries().get(13),instance.getContinentByName(" europa").getCountries().get(36));
        Assert.assertEquals(expectedInt, i);
    }
}


