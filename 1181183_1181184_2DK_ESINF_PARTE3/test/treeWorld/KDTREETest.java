package treeWorld;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class KDTREETest {

    private treeWorld.KDTREE instance;
    private List<Country> countries;

    public KDTREETest() throws IOException {
        this.instance = new KDTREE();
        instance.createTree();
        List<String> p = Files.lines(Paths.get("paises.txt")).collect(Collectors.toList());
        List<String> f = Files.lines(Paths.get("fronteiras.txt")).collect(Collectors.toList());
        countries = Utils.updateCountriesWithBorders(p,f);
    }

    @Test
    public void getCountryByCoordinates() {
        System.out.println("2D-TREE ordenada por latitude e longitude");
        System.out.println(instance);
        System.out.println("-----TESTE ALÍNEA 2B - PESQUISA EXATA-----");
        System.out.println("TESTE 1");
        Double lat = -34.90328;
        Double lon = -56.18816;
        System.out.println("Coordenadas - Latitude: "+lat+" Longitude: "+lon);
        Country expected = Utils.getCountryByName("uruguai", (LinkedList<Country>) countries);
        Country result = instance.getCountryByCoordinates(lat,lon);
        System.out.println("País esperado: "+expected.getName());
        System.out.println("País resultado: "+result.toString_Coordinates());
        assertEquals(expected, result);
        System.out.println("TESTE 2");
        lat = 41.8954656;
        lon = 12.4823243;
        System.out.println("Coordenadas - Latitude: "+lat+" Longitude: "+lon);
        expected = Utils.getCountryByName("italia", (LinkedList<Country>) countries);
        result = instance.getCountryByCoordinates(lat,lon);
        System.out.println("País esperado: "+expected.getName());
        System.out.println("País resultado: "+result.toString_Coordinates());
        assertEquals(expected, result);
    }

    @Test
    public void getClosestNeighbor() {
/*        int y = 0;
        for (int i = -175; i < 175; i++) {
            for (int j = -85; j < 85; j++) {

                double min = Double.MAX_VALUE;
                Country a = null;
                for (Country country : instance.inOrder()) {
                    double s  =Utils.calculateDistance(i,j, country.getCoordinates().getLatitude(), country.getCoordinates().getLongitude());
                    if (s<min){
                        min = s;
                        a  =country;
                    }
                }
                if (!a.equals(instance.getClosestNeighbor(i,j))){
                    y++;

                    System.out.println(KDTREE.n);
                }



            }

        }*/
        System.out.println("-----TESTE ALÍNEA 2C - ENCONTRAR O PAÍS VIZINHO MAIS PERTO DA COORDENADA RECEBIDA-----");
        System.out.println("TESTE 1");
        Double lat = 54.57206165565852;
        Double lon = 4.394531250000001;
        System.out.println("Coordenadas: latitude- "+lat+" longitude- "+lon);
        Country expected = Utils.getCountryByName("holanda", (LinkedList<Country>) countries);
        System.out.println("País esperado: " + expected.getName());
        Country result = instance.getClosestNeighbor(lat, lon);
        System.out.println("País resultado: " + result.getName());
        assertEquals(expected, result);
    }

    @Test
    public void searchGeographicArea() {
        System.out.println("-----TESTE ALÍNEA 2D - ENCONTRAR A LISTA DE PAÍSES PERTENCENTES A UMA ZONA RETANGULAR DEFINIDA POR 2 COORDENADAS-----");
        System.out.println("TESTE 1");
        Coordinates c1 = new Coordinates(43.61221676817573, -10.415039062500002);
        System.out.println("Coordenadas 1: Latitude- " + c1.getLatitude() + " longitude- " + c1.getLongitude());
        Coordinates c2 = new Coordinates(36.10237644873644,-6.284179687500001);
        System.out.println("Coordenadas 2: Latitude- " + c2.getLatitude() + " longitude- " + c2.getLongitude());
        LinkedList<Country> expectedResult = new LinkedList<>();
        expectedResult.add(Utils.getCountryByName("portugal", (LinkedList<Country>) countries));
        LinkedList<Country> realResult = instance.searchGeographicArea(c1,c2);
        System.out.println("Resultados expectáveis:");
        for(int i = 0; i < expectedResult.size(); i++){
            System.out.println("País " + i+1 + ": " + expectedResult.get(i).getName());
        }
        System.out.println("Resultados reais:");
        for(int i = 0; i < realResult.size(); i++){
            System.out.println("País " + i+1 + ": " + realResult.get(i).getName());
        }
        assertEquals(expectedResult, realResult);
    }
}