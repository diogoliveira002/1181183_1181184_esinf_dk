package treeWorld;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class TREE_WORLDTest {

    private treeWorld.TREE_WORLD instance1;
    private TREE_WORLD instance2;
    private List<Country> countries;

    public TREE_WORLDTest() throws IOException {
        this.instance1 = new treeWorld.TREE_WORLD();
        this.instance2 = new TREE_WORLD();
        instance1.createTree();
        List<String> p = Files.lines(Paths.get("paises.txt")).collect(Collectors.toList());
        List<String> f = Files.lines(Paths.get("fronteiras.txt")).collect(Collectors.toList());
        countries = Utils.updateCountriesWithBorders(p,f);
        instance2.createTree2();

    }

    @Test
    public void getBordersFromCountry() {
        System.out.println("ÁRVORE ORDENADA ALFABETICAMENTE:");
        System.out.println(instance1);
        System.out.println("---- TESTE ALÍNEA 1a ----");
        System.out.println("TESTE 1:");
        System.out.println("PAÍS 1: PORTUGAL");
        String nameOfCountry = "portugal";
        System.out.println();
        List<Country> result = new LinkedList<>();
        for(Country c : countries) {
            if (c.getName().equalsIgnoreCase(nameOfCountry)) {
                result = c.getBorders();
            }
        }
        List<Country> expected = instance1.getBordersFromCountry(nameOfCountry);
        System.out.println("Valor esperado: "+expected);
        System.out.println("Valor resultado: "+result);
        assertEquals(expected, result);

        System.out.println("\n \nTESTE 2:");
        System.out.println("PAÍS 2: HOLANDA");
        nameOfCountry = "holanda";
        System.out.println();
        for(Country c : countries) {
            if (c.getName().equalsIgnoreCase(nameOfCountry)) {
                result = c.getBorders();
            }
        }
        expected = instance1.getBordersFromCountry(nameOfCountry);
        System.out.println("Valor esperado: "+expected);
        System.out.println("Valor resultado: "+result);
        assertEquals(expected, result);
    }

    @Test
    public void inOrderSelectingCountry() {
        System.out.println("---- TESTE ALÍNEA 1b ----");
        System.out.println("ÁRVORE BINÁRIA ORDENADA POR NÚMERO CRESCENTE DE FRONTEIRAS E POPULAÇÃO: \n");
        System.out.println(instance2);
        System.out.println("---- TESTE 1 ----");
        System.out.println("América do Sul - Lista de países ordenada \n");
        String continent = "americasul";
        List<Country> listCountries = new LinkedList<>();
        Iterable<Country> resulta = instance2.inOrderSelectingCountry(continent);
        resulta.forEach(listCountries::add);
        for(Country c : listCountries) {
            System.out.println(c);
        }
        boolean result = true;
        boolean expected = isInOrder(listCountries, continent);
        System.out.println();
        assertEquals(expected, result);
        System.out.println("---- TESTE 2 ----");
        System.out.println("Europa - Lista de países ordenada \n");
        continent = "europa";
        resulta = instance2.inOrderSelectingCountry(continent);
        listCountries.clear();
        resulta.forEach(listCountries::add);
        for(Country c : listCountries) {
            System.out.println(c);
        }
        System.out.println();
        expected = isInOrder(listCountries, continent);
        assertEquals(expected, result);


    }

    private boolean isInOrder(List<Country> listCountries, String continent) {
        boolean expected = false;
        for (int i = 0; i < listCountries.size(); i++) {
            int j = i + 1;
            if (j < listCountries.size()) {
                Country a = listCountries.get(i);
                Country b = listCountries.get(j);
                if (a.getContinent().equalsIgnoreCase(continent) && b.getContinent().equalsIgnoreCase(continent)) {
                    if (a.getNumOfBorders() > b.getNumOfBorders()) {
                        expected = true;
                    }
                    if (a.getNumOfBorders() == b.getNumOfBorders()) {
                        if (a.getPopulation() <= b.getPopulation()) {
                            expected = true;
                        } else {
                            expected = false;
                            break;
                        }
                    }
                    if (a.getNumOfBorders() < b.getNumOfBorders()) {
                        expected = false;
                        break;
                    }
                } else {
                    expected = false;
                }
            }
        }
        return expected;
    }

}