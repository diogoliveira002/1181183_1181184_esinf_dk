package treeWorld;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Utils {

    private static LinkedList<Country> createCountries(List<String> countryList) {
        LinkedList<Country> countries = new LinkedList<>();
        for (int i = 0; i < countryList.size(); i++) {
            String[] aux1 = countryList.get(i).trim().split(",");
            trimArray(aux1);
            Coordinates coordinates = new Coordinates(Double.parseDouble(aux1[4]), Double.parseDouble(aux1[5]));
            Country country = new Country(aux1[0], aux1[1], aux1[3], Double.parseDouble(aux1[2]), coordinates);
            countries.add(country);
        }
        return countries;
    }

    public static LinkedList<Country> updateCountriesWithBorders(List<String> countryList, List<String> bordersList) {
        LinkedList<Country> countries = createCountries(countryList);
        for (Country c : countries) {
            LinkedList<Country> borders = createBordersList(bordersList, c, countries);
            c.setBorders(borders);
            c.setNumOfBorders(borders.size());
        }
        return countries;
    }

    private static LinkedList<Country> createBordersList(List<String> bordersList, Country country, LinkedList<Country> allCountries) {
        ListIterator<String> li = bordersList.listIterator();
        LinkedList<Country> borders = new LinkedList<>();
        while (li.hasNext()) {
            String[] aux = li.next().trim().split(",");
            trimArray(aux);
            if (country.getName().equalsIgnoreCase(aux[0])) {
                for (Country c : allCountries) {
                    if (c.getName().equalsIgnoreCase(aux[1])) {
                        borders.add(c);
                    }
                }
            }
            if (country.getName().equalsIgnoreCase(aux[1])) {
                for (Country c : allCountries) {
                    if (c.getName().equalsIgnoreCase(aux[0])) {
                        borders.add(c);
                    }
                }
            }
        }
        return borders;
    }

    public static Country getCountryByName(String name, LinkedList<Country> countries) {
        for (Country c : countries) {
            if (c.getName().equalsIgnoreCase(name)) {
                return c;
            }
        }
        return null;
    }



    /**
     * trim blank spaces from array. Example "portugal " to "portugal"
     * @param array some array to trim spaces
     */
    public static void trimArray(String[] array) {
        for (int i = 0; i < array.length; i++)
            array[i] = array[i].trim();
    }

    /**
     * calculate distance between two points in the world, given their latitudes and longitudes
     * @param lat1 latitude from country1
     * @param lon1 longitude from country1
     * @param lat2 latitude from country2
     * @param lon2 longitude from country2
     * @return distance between country1 and country 2, in kilometers
     */
    public static double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        final double R = 6371e3;
        double theta1 = Math.toRadians(lat1);
        double theta2 = Math.toRadians(lat2);
        double deltaTheta = Math.toRadians(lat2 - lat1);
        double deltaLambda = Math.toRadians(lon2 - lon1);
        double a = Math.sin(deltaTheta / 2) * Math.sin(deltaTheta / 2) + Math.cos(theta1)
                * Math.cos(theta2) * Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c; //distância em metros
        return Math.round(d * Math.pow(10, -3));// devolve em quilometros
    }
}
