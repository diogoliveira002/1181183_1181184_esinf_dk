package treeWorld;

import treeBase.BST;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class TREE_WORLD extends BST<Country> {

    // create BST ordered by alphabetical order
    public void createTree() throws IOException {
        List<String> p = Files.lines(Paths.get("paises.txt")).collect(Collectors.toList());
        List<String> f = Files.lines(Paths.get("fronteiras.txt")).collect(Collectors.toList());
        LinkedList<Country> countries = Utils.updateCountriesWithBorders(p,f);
        for (Country country : countries) {
            insert(country);
        }
    }

    // create BST ordered by number of frontiers and population
    public void createTree2() throws IOException {
        List<String> p = Files.lines(Paths.get("paises.txt")).collect(Collectors.toList());
        List<String> f = Files.lines(Paths.get("fronteiras.txt")).collect(Collectors.toList());
        LinkedList<Country> countries = Utils.updateCountriesWithBorders(p,f);
        for (Country country : countries) {
            insert2(country);
        }
    }

    // adaptation of insert method to the BST ordered by the number of borders and population
    public void insert2(Country element){
        root = insert2(element, root);
    }

    // recursive method to insert nodes in BST
    private Node<Country> insert2(Country element, Node<Country> node){
        if (node == null) {
            return new Node(element, null, null);
        }
        if (node.getElement() == element) {
            node.setElement(element);
        } else {
            // if element to insert has more borders than the actual node, it will be inserted left
            if(node.getElement().getNumOfBorders()<element.getNumOfBorders()) {
                node.setLeft(insert2(element, node.getLeft()));
                // if element to insert has less borders than the actual node, it will be inserted left
            } else if (node.getElement().getNumOfBorders()>element.getNumOfBorders()){
                node.setRight(insert2(element, node.getRight()));
                // if element to insert has the same number of borders than the actual node, it will be inserted left
            } else {
                // if element to insert has higher population than the actual node, it will be inserted left
                if(node.getElement().getPopulation()>element.getPopulation()) {
                    node.setLeft(insert2(element, node.getLeft()));
                    // if element to insert has lower or same population than the actual node, it'll be inserted right
                } else {
                    node.setRight(insert2(element, node.getRight()));
                }
            }
        }
        return node;

    }


    public Iterable<Country> inOrderSelectingCountry (String continent){
        List<Country> snapshot = new ArrayList<>();
        if (root!=null)
            inOrderSubtree(root, snapshot, continent);   // fill the snapshot recursively
        return snapshot;
    }
    /**
     * Adds elements of the subtree rooted at Node node to the given
     * snapshot using an in-order traversal
     * @param node       Node serving as the root of a subtree
     * @param snapshot  a list to which results are appended
     */
    private void inOrderSubtree(Node<Country> node, List<Country> snapshot, String continent) {
        if (node == null)
            return;
        inOrderSubtree(node.getLeft(), snapshot, continent);
        // if the element of the node has the inserted continent, adds to the list
        if (node.getElement().getContinent().equalsIgnoreCase(continent)) {
            snapshot.add(node.getElement());
        }
        inOrderSubtree(node.getRight(), snapshot, continent);
    }

    public List<Country> getBordersFromCountry(String c) {
        Node<Country> node = find(c, root);
        return node.getElement().getBorders();
    }

    //find by name of country
    protected Node<Country> find(String element, Node<Country> node) {
        if (node == null) {
            return null;
        }
        // if name of the element is the same as element, return node
        if (node.getElement().getName().compareTo(element) == 0) {
            return node;
        }
        // if name of the element is after (alphabetically) element, recursively calls method to the left
        if (node.getElement().getName().compareTo(element) > 0) {
            return find(element, node.getLeft());
        }
        // if name of the element is before (alphabetically) element, recursively calls method to the right
        return find(element, node.getRight());
    }
}
