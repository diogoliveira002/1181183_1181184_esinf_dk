package treeWorld;

import treeBase.BST;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class KDTREE extends BST<Country> {
    public static int n;

    public void createTree() throws IOException {
        List<String> p = Files.lines(Paths.get("paises.txt")).collect(Collectors.toList());
        List<String> f = Files.lines(Paths.get("fronteiras.txt")).collect(Collectors.toList());
        LinkedList<Country> countries = Utils.updateCountriesWithBorders(p,f);
        for (Country country : countries) {
            insert(country);
        }
    }

    public void insert(Country element){

        root = insert(element, root(), 0);
    }

    // insert nodes by latitude (even level) and longitude (odd level)
    private Node<Country> insert(Country element, Node<Country> node, int level) {
        if (node == null) {
            return new Node(element, null, null);
        }
        if (node.getElement() == element) {
            node.setElement(element);
        } else {
            // if level is even, compare latitudes
            if (level %2 == 0) {
                // if latitude of the node is bigger than the latitude of the element, recursively calls method, updating node.getLeft() and increasing the level
                if(node.getElement().compareLatitude(element)>0) {
                    node.setLeft(insert(element, node.getLeft(), level+1));
                    // if latitude of the node is lesser or equal than the latitude of the element, recursively calls method, updating node.getRight() and increasing the level
                } else {
                    node.setRight(insert(element, node.getRight(), level+1));
                }
                // if level is odd, compare longitudes
            } else {
                // if longitude of the node is bigger than the longitude of the element, recursively calls method, updating node.getLeft() and increasing the level
                if(node.getElement().compareLongitude(element)>0) {
                    node.setLeft(insert(element, node.getLeft(), level+1));
                    // if longitude of the node is lesser or equal than the longitude of the element, recursively calls method, updating node.getLeft() and increasing the level
                } else {
                    node.setRight(insert(element, node.getRight(), level+1));
                }
            }
        }
        return node;
    }

    /**
     * return country of coordinates lat, lon, or null if no country is found
     * @param lat latitude of the country we are searching for
     * @param lon longitude of the country we are searching for
     * @return country with lat as latitude and lon as longitude
     */
    protected Country getCountryByCoordinates(double lat, double lon) {
        // find used to search in all tree
        Node<Country> node = find(lat, lon, root(), 0);
        if (node == null) {
            return null;
        }
        return node.getElement();
    }

    /**
     * Adaptation of find method of BST class, to search a country given its coordinates
     * @param lat of country
     * @param lon of country
     * @param node node
     * @param level level of the node
     * @return Node<Country> country, of coordinates lat, lon
     */
    protected Node<Country> find(double lat, double lon, Node<Country> node, int level){
        if (node == null) {
            return null;
        }
        // if the latitude and the longitude of the node are equal to the ones given, return node
        if (node.getElement().compareLongitude(lon) == 0 && node.getElement().compareLatitude(lat) == 0) {
            return node;
        }
        // if level is even
        if(level%2 == 0) {
            // if the latitude of the element is greater than the lat received by parameter, recursively calls method, going to the left node
            if (node.getElement().compareLatitude(lat) > 0) {
                return find(lat, lon , node.getLeft(), level+1);
            }
            // if the latitude of the element is lesser than the lat received by parameter, recursively calls method, going to the right node
            return find(lat, lon, node.getRight(), level+1);
            // if level is odd
        } else {
            // if the longitude of the element is greater than the lon received by parameter, recursively calls method, going to the left node
            if (node.getElement().compareLongitude(lon) > 0) {
                return find(lat, lon, node.getLeft(), level+1);
            }
            // if the longitude of the element is lesser than the lon received by parameter, recursively calls method, going to the right node
            return find(lat, lon, node.getRight(), level+1);
        }
    }

    /**
     * return the closest country to the coordinates latitude lat and longitude lon
     * @param lat latitude
     * @param lon longitude
     * @return the closest country of the coordinates received
     */
    protected Country getClosestNeighbor(double lat, double lon) {
        return getClosestNeighborAux(root, lat, lon, root.getElement(), true);
    }

    /**
     * Recursive method that calls him self until it gets the closest country comparing the distance from the present node with the distance from champion node to the coordinates lat and lon
     * @param node present node that is being verified if is the closest country to the coordinates lat and lon
     * @param lat latitude of the coordinates
     * @param lon longitude of the coordinates
     * @param champion closest node to the coordinates lat and lon at the present
     * @param evenLevel boolean variable that says if the level of the node is even or odd
     * @return returns the closest country to the coordinates lat latitude and lon longitude
     */
    private Country getClosestNeighborAux(Node<Country> node, double lat, double lon, Country champion, boolean evenLevel){

        if(node == null){
            return champion;
        }

        if(node.getElement().getCoordinates().getLatitude() == lat && node.getElement().getCoordinates().getLongitude() == lon){
            return node.getElement();
        }

        if(Utils.calculateDistance(lat,lon,node.getElement().getCoordinates().getLatitude(), node.getElement().getCoordinates().getLongitude()) < Utils.calculateDistance(lat,lon,champion.getCoordinates().getLatitude(), champion.getCoordinates().getLongitude())){
            champion = node.getElement();
        }

        double distanceLine = this.comparePoints(lat, lon, node, evenLevel);

        if(distanceLine < 0){
            champion = KDTREE.this.getClosestNeighborAux(node.getLeft(), lat, lon, champion, !evenLevel);
            if(Utils.calculateDistance( lat, lon, champion.getCoordinates().getLatitude(), champion.getCoordinates().getLongitude()) >= distanceLine ){
                champion = KDTREE.this.getClosestNeighborAux(node.getRight(), lat, lon, champion, !evenLevel);
            }
        }
        else {
            champion = KDTREE.this.getClosestNeighborAux(node.getRight(), lat, lon, champion, !evenLevel);

            // Since champion may have changed, recalculate distance
            if(Utils.calculateDistance( lat, lon, champion.getCoordinates().getLatitude(), champion.getCoordinates().getLongitude()) >= distanceLine){
                champion = KDTREE.this.getClosestNeighborAux(node.getLeft(), lat, lon, champion, !evenLevel);
            }
        }

        return champion;
    }

    /**
     * returns the difference of the latitude of the coordinate with the node latitude or the difference of the longitude of the coordinate with the longitude of the node
     * @param lat latitude of the coordinate
     * @param lon longitude of the coordinate
     * @param node node with the latitude and longitude that will be used
     * @param evenLevel says if the level of the node is even or odd
     * @return the difference of the latitude of the coordinate with the node latitude or the difference of the longitude of the coordinate with the longitude of the node
     */
    private double comparePoints(double lat, double lon, Node<Country> node, boolean evenLevel){
        if(evenLevel){
            return lat - node.getElement().getCoordinates().getLatitude();
        }
        else return lon - node.getElement().getCoordinates().getLongitude();
    }

    /**
     * method that returns the list of countries that are in the rectangle zone defined by coordinates p1 and p2
     * @param p1 coordinate 1 that defines the rectangle
     * @param p2 coordinate 2 that defines the rectangle
     * @return list of countries that are in the rectangle zone defined by coordinates p1 and p2
     */
    protected LinkedList<Country> searchGeographicArea(Coordinates p1, Coordinates p2){
        LinkedList<Country> listCountry = new LinkedList<>();
        if(p1.getLatitude() > p2.getLatitude()){
            double lat = (double) (p1.getLatitude() - ((p1.getLatitude() - p2.getLatitude()) / 2));
            double lon = (double) (p2.getLongitude() - ((p2.getLongitude() - p1.getLongitude()) / 2));
            Coordinates ret = new Coordinates(lat, lon);
            searchGeographicAreaAux(root, p1, p2, ret,listCountry, true);
        }else{
            double lat = (double) (p2.getLatitude() - ((p2.getLatitude() - p1.getLatitude()) / 2));
            double lon = (double) (p1.getLongitude() - ((p1.getLongitude() - p2.getLongitude()) / 2));
            Coordinates ret = new Coordinates(lat, lon);
            searchGeographicAreaAux(root, p1, p2, ret,listCountry, true);
        }
        return listCountry;
    }

    /**
     * Adds to the listCountry the countries that are in the rectangle defined by the coordinates p1 and p2
     * @param node present node that is verified being in the rectangle zone defined by p1 and p2
     * @param p1 coordinate 1 that defines the rectangle zone
     * @param p2 coordinate 2 that defines the rectangle zone
     * @param ret center coordinates of the rectangle zone
     * @param listCountry list that saves the countries that are in the rectangle zone defined by p1 and p2
     * @param evenLevel defines if the level of the node is even or odd
     */
    private void searchGeographicAreaAux(Node<Country> node, Coordinates p1, Coordinates p2, Coordinates ret, LinkedList<Country> listCountry, boolean evenLevel){
        double maxLat = Double.MAX_VALUE;
        double maxLon = Double.MAX_VALUE;
        double minLat = Double.MAX_VALUE;
        double minLon = Double.MAX_VALUE;

        if (node == null){
            return;
        }

        double nodeLat = node.getElement().getCoordinates().getLatitude();
        double nodeLon = node.getElement().getCoordinates().getLongitude();

        if(p1.getLatitude()>p2.getLatitude()){
            maxLat = p1.getLatitude();
            minLat = p2.getLatitude();
            maxLon = p2.getLongitude();
            minLon = p1.getLongitude();
        }else{
            maxLat = p2.getLatitude();
            minLat = p1.getLatitude();
            maxLon = p1.getLongitude();
            minLon = p2.getLongitude();
        }

        if(nodeLat<= maxLat && nodeLat >= minLat && nodeLon <= maxLon && nodeLon >= minLon){
            listCountry.add(node.getElement());
        }

        double distanceLine = comparePoints(ret.getLatitude(), ret.getLongitude(), node, evenLevel);

        if(distanceLine < 0){
            searchGeographicAreaAux(node.getLeft(), p1, p2, ret, listCountry, !evenLevel);
            if(Utils.calculateDistance(ret.getLatitude(), ret.getLongitude(), nodeLat, nodeLon) >= distanceLine){
                searchGeographicAreaAux(node.getRight(), p1, p2, ret, listCountry, !evenLevel);
            }
        }
        else {
            searchGeographicAreaAux(node.getRight(), p1, p2, ret, listCountry, !evenLevel);
            if(Utils.calculateDistance(ret.getLatitude(), ret.getLongitude(), nodeLat, nodeLon) >= distanceLine){
                searchGeographicAreaAux(node.getLeft(), p1, p2, ret, listCountry, !evenLevel);
            }
        }
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        toStringRec(root, 0, sb);
        return sb.toString();
    }

    private void toStringRec(Node<Country> root, int level, StringBuilder sb){
        if(root==null)
            return;
        toStringRec(root.getRight(), level+1, sb);
        if (level!=0){
            for(int i=0;i<level-1;i++)
                sb.append("|\t");
            sb.append("|-------"+root.getElement().toString_Coordinates()+"\n");
        }
        else
            sb.append(root.getElement().toString_Coordinates()+"\n");
        toStringRec(root.getLeft(), level+1, sb);
    }
}

