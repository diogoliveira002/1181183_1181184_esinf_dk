package treeWorld;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Country implements Comparable<Country> {

    private String name;
    private String capital;
    private String continent;
    private double population;
    private Coordinates coordinates;
    private List<Country> borders;
    private int numOfBorders;

    public Country(String name, String continent, String capital, double population, Coordinates coordinates) {
        this.name = name;
        this.continent = continent;
        this.capital = capital;
        this.population = population;
        this.coordinates = coordinates;
        this.borders = new LinkedList<>();
        this.numOfBorders = 0;
    }

    public String getName(){
        return name;
    }

    public String getContinent() {
        return continent;
    }

    public String getCapital(){
        return capital;
    }

    public double getPopulation(){
        return population;
    }

    public Coordinates getCoordinates(){
        return coordinates;
    }

    public List<Country> getBorders() {
        return borders;
    }

    public int getNumOfBorders() {
        return numOfBorders;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public void setCapital(String capital){
        this.capital = capital;
    }

    public void setPopulation(double population){
        this.population = population;
    }

    public void setCoordinates(Double longitude, Double latitude){
        this.coordinates.setLongitude(longitude);
        this.coordinates.setLatitude(latitude);
    }

    public void setBorders(List<Country> borders) {
        this.borders = borders;
    }

    public void setNumOfBorders(int numOfBorders) {
        this.numOfBorders = numOfBorders;
    }

    public double compareLatitude(Country o) {
        return  this.getCoordinates().getLatitude().compareTo(o.getCoordinates().getLatitude());
    }

    public double compareLongitude(Country o) {
        return this.getCoordinates().getLongitude().compareTo(o.getCoordinates().getLongitude());
    }

    public int compareLatitude(Double o) {
        return Double.compare(this.getCoordinates().getLatitude(), o);
    }

    public int compareLongitude(Double o) {
        return Double.compare(this.getCoordinates().getLongitude(), o);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return Double.compare(country.population, population) == 0 &&
                numOfBorders == country.numOfBorders &&
                Objects.equals(name, country.name) &&
                Objects.equals(capital, country.capital) &&
                Objects.equals(continent, country.continent) &&
                Objects.equals(coordinates, country.coordinates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, capital, continent, population, coordinates, borders, numOfBorders);
    }

    /*@Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Country{");
        sb.append("name='").append(name).append('\'');
        sb.append(", capital='").append(capital).append('\'');
        sb.append(", continent='").append(continent).append('\'');
        sb.append(", population=").append(population);
        sb.append(", coordinates=").append(coordinates);
        //sb.append(", borders=").append(borders);
        sb.append(", numOfBorders=").append(numOfBorders);
        sb.append('}');
        return sb.toString();
    }*/

    @Override
    public String toString() {
        return this.name + " Borders: " + this.getNumOfBorders() + " Population: " + this.getPopulation();
    }


    public String toString_Coordinates() {
        return this.name + " Coordinates: " + this.getCoordinates().getLatitude() + "," + this.getCoordinates().getLongitude();
    }


    @Override
    public int compareTo(Country o) {
        return name.compareTo(o.getName());
    }
}
